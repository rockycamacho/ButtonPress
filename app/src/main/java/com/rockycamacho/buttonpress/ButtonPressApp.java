package com.rockycamacho.buttonpress;

import android.app.Application;
import android.content.Context;

import com.frogermcs.androiddevmetrics.AndroidDevMetrics;
import com.rockycamacho.buttonpress.di.AppComponent;
import com.rockycamacho.buttonpress.di.AppModule;
import com.rockycamacho.buttonpress.di.DaggerAppComponent;

import timber.log.Timber;

/**
 * Created by Rocky Camacho on 3/28/2016.
 */
public class ButtonPressApp extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        if(BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
            AndroidDevMetrics.initWith(this);
        }
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static ButtonPressApp get(Context context) {
        return (ButtonPressApp) context.getApplicationContext();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}

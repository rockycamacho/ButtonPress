package com.rockycamacho.buttonpress.di;

import dagger.Component;

/**
 * Created by Rocky Camacho on 3/28/2016.
 */
@Component(modules = AppModule.class)
public interface AppComponent {
}
